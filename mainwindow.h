#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

    void on_actionExit_2_triggered();

    void on_actionAbout_LasagnaTextEditor_triggered();

    void on_tabWidget_tabCloseRequested(int index);
private:
    Ui::MainWindow *ui;
    QString file_path;
};

#endif // MAINWINDOW_H

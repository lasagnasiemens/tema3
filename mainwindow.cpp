#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <qtextedit.h>
#include <QtGui>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->tabWidget);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_triggered()
{
    ui->tabWidget->addTab(new QTextEdit(), QString("New %0").arg(ui->tabWidget->count()+1));
    file_path="";
    ui->textEdit->setText("");
}

void MainWindow::on_actionOpen_triggered()
{
 QString file_name = QFileDialog::getOpenFileName(this,"Open a file");
 QFile file (file_name);
 file_path=file_name;

  if(!file.open(QFile::ReadOnly | QFile::Text)){
    QMessageBox::critical(this,"Error Opening File","File Cannot be Opened!");
    return;
  }
  else{
    QTextStream inputData(&file);
    QString fileText = inputData.readAll();
    ui->textEdit->setHtml(fileText);
    file.close();
  }
}

void MainWindow::on_actionSave_triggered()
{
     QFile file(file_path);
    if(!file.open(QFile::WriteOnly | QFile::Text)){
      return;
    }
    else{
      QTextStream writeData(&file);
      QString fileText = ui->textEdit->toPlainText();
      writeData << fileText;
      file.flush();
      file.close();
    }
}

void MainWindow::on_actionSave_as_triggered()
{
    QString file_name = QFileDialog::getSaveFileName(this,"Open a file");
    QFile file (file_name);
    file_path = file_name;
    if(!file.open(QFile::WriteOnly | QFile::Text)){
        return;
     }
     else{
       QTextStream writeData(&file);
       QString fileText = ui->textEdit->toPlainText();
       writeData << fileText;
       file.flush();
       file.close();
     }
}

void MainWindow::on_actionExit_2_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionAbout_LasagnaTextEditor_triggered()
{
    QMessageBox::about(this,"About TextEditor"," Copyright \nMargarit Marian Catalin\nAnghel Mihai Alexandru");
}
void MainWindow::on_tabWidget_tabCloseRequested(int index)
{
    ui->tabWidget->removeTab(index);
}
